#!/bin/bash
#Thibault Saint-Léger

sudo apt update 
sudo apt-get update
echo "Installer Java ? (o/n)"
read rep
if [ $rep  = "o" ]
then
  sudo apt install default-jdk
  echo "Voulez-vous exécuter le fichier Executable.java ? (o/n)"
  read rep
  if [ $rep = "o" ]
  then
    touch Executable.java
    cat <<EOF >  Executable.java

    public 
    class Executable{
     
        public 
    static void main(String[] args){

              System.out.println(" Hello World !") ;
      }
    }
EOF
    javac Executable.java
    java Executable
  fi
fi

echo "Installer Python ? (o/n)"
read rep
if [ $rep = "o" ]
then
  sudo apt install python3
  echo "Voulez-vous exécuter le fichier test.py ? (o/n)"
  read rep
  if [ $rep = "o" ]
  then
    touch test.py
    cat <<EOF > test.py
print("Hello World")
EOF
    python3 test.py
  fi
fi

echo "Installler Visual Studio Code ? (o/n)"
read rep
if [ $rep = "o" ]
then
  sudo apt install software-properties-common apt-transport-https wget
  wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
  sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
  sudo apt install code
  code --install-extension ms-python.python
  code --install-extension njpwerner.autodocstring
  code --install-extension ms-python.vscode-pylance
  code --install-extension dongli.python-preview
  code --install-extension ms-ceintl.vscode-language-pack-fr
  echo "Vs Code est installé avec Python plus ses extensions commme Auto-docstring, langue en français"
fi

echo "Installer Docker ? (o/n)"
read rep
if [ $rep = "o" ]
then
  sudo apt install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
  echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  sudo curl -sSL https://get.docker.com/ | sh
  sudo docker run hello-world
fi

echo "Voulez-vous créer des dossiers? (o/n)"
read rep
if [ $rep = "o" ]
then 
  mkdir "Java" " Python" "Visual Stuido Code" "Docker" "Oracle" "Bash" "Eco-durable-et-numerique" "Anglais"
  echo "Les dossiers ont été créé."
fi
