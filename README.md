# Rendu SAE Bash - `Bash`

## Introduction

- Le script bash fournis installerra un environnement de développement en fonction de vos besoins. Ainsi, vous pourrez développer en `Python`, `Java`, utiliser `Visual Studio Code` ainsi que `Docker`.

## Mon choix

- Pour cette SAE, j'ai choisis de faire une installation de xubuntu en "double boot" sur ma machine avec des installations et paramétrages logiciels.

## Utilité du script

- le script vous posera des questions au fur et à mesures de sa lecture pour savoir ce que vous voulez **installer** et **configurer** en fonction de vos besoins. 
- Puis votre environnement de développement aura été configurer comme vous l'aurez souhaité.

## Reproduire mon installation

- Pour reproduire mon installation il vous suffira d'importer mon script sur votre machine. Et de lancer mon script dans le répertoire de votre choix.
- Pour lancer mon script, il vous faudra d'abord lui donner les permissions adéquates en tappant dans votre terminal **chmod u+x test.sh**
- Le script vous expliquera ce qu'il fait sur votre distribution Linux.
Pour chacune des questions posées répondez correctement (soit `o` soit `n`).

**PS: Tout ce qui sera différent de `o` sera compté comme `n`.**

- A la fin de toutes les questions, le script aura installer ce que vous avez demandé.

### PS
Voici le lien pour mon gitlab : https://gitlab.com/Zenido/sae-bash.git

Vous y trouverez la vidéo. La vidéo prenant trop de place, je ne l'ai pas mis dans le .zip